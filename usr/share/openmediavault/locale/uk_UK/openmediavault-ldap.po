# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: openmediavault\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-08-19 16:20+0200\n"
"PO-Revision-Date: 2012-08-19 14:21+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Ukrainian (http://www.transifex.com/projects/p/openmediavault/language/uk/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: uk\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

msgid "Base DN"
msgstr ""

msgid "Directory Service"
msgstr ""

msgid "Enable"
msgstr ""

msgid "Extra options"
msgstr ""

msgid "General settings"
msgstr ""

msgid "Groups suffix"
msgstr ""

msgid "Host"
msgstr ""

msgid "LDAP"
msgstr ""

msgid "Password"
msgstr ""

msgid "Root Bind DN"
msgstr ""

msgid ""
"Specifies the base distinguished name (DN) to use as search base, e.g. "
"'dc=example,dc=net'."
msgstr ""

msgid "Specifies the credentials with which to bind."
msgstr ""

msgid ""
"Specifies the distinguished name (DN) with which to bind to the directory "
"server for lookups, e.g. 'cn=manager,dc=example,dc=net'."
msgstr ""

msgid "Specifies the group suffix, e.g. 'ou=Groups'."
msgstr ""

msgid "Specifies the user suffix, e.g. 'ou=Users'."
msgstr ""

msgid "The FQDN or IP address of the server."
msgstr ""

msgid "Users suffix"
msgstr ""
